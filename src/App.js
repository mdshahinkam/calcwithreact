import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const buttonTypes = {
  n : 'NumButton',
  o : 'OpButton',
  p : 'PuncButton'
}

function removeLeadingZero(tempResult){
  {/* To avoid octal literals */}
  if(tempResult.length&&tempResult.charAt(0)==="0"){
    tempResult = tempResult.substr(1);
  }
  return tempResult;
}
function CalcDisplay(props){
    return(
      <div className="Result-Display">{props.result}</div>
    );
  }
class Button extends Component{
  constructor(props){
    super(props);
    this.handleInput = this.handleInput.bind(this);
  }
  handleInput(e){
    this.props.onButtonClick(e,this.props);
  }

  render(){
    const buttonType = buttonTypes[this.props.butType];
    const buttonId = this.props.butVal;
    var buttonValue;
    switch(buttonType){
      case "NumButton":
        buttonValue = buttonId;
        break;

      case "PuncButton":
        buttonValue = buttonId === "openParanth" ? "(" : ")";
        break;

      default:
        switch(buttonId){
          case "add":
            buttonValue = "+";
            break;

          case "sub":
            buttonValue = "-";
            break;

          case "mul":
            buttonValue = "*";
            break;

          case "div":
            buttonValue = "/";
            break;

          case "eql":
            buttonValue = "=";
            break;

          default:
            buttonValue = "C";
            break;
        }
        break;
    }
    return (
      <button className={buttonType} onClick={this.handleInput} id={buttonId}>{buttonValue}</button>
    );
  };
}
class App extends Component {
  constructor(props){
    super(props);
    this.state = {result:"0"};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }


  handleChange(event) {
    const newResult = removeLeadingZero(event.target.value);
    this.setState({result: newResult});
  }
  handleSubmit(event) {
    event.preventDefault();
    const newResult = eval(removeLeadingZero(this.state.result));
    this.setState( {
      result: newResult
    });
  }
  handleButtonClick(e,props){
    const clickedId = e.target.id;
    const buttonType = buttonTypes[props.butType];
    const newResult = removeLeadingZero(this.state.result);
    this.setState({
      result: newResult
    });
    switch(buttonType){
      case "NumButton":
        this.setState( prevState => ({
          result: prevState.result + clickedId
        }));
        break;

      case "PuncButton":
        var punc = clickedId === "openParanth"? "(" : ")";
        this.setState( prevState => ({
          result: prevState.result + punc
        }));
        break;

      default:
        switch(clickedId){
          case "add":
          this.setState( prevState => ({
            result: prevState.result + "+"
          }));
            break;

          case "sub":
          this.setState( prevState => ({
            result: prevState.result + "-"
          }));
            break;

          case "mul":
          this.setState( prevState => ({
            result: prevState.result + "*"
          }));
            break;

          case "div":
          this.setState( prevState => ({
            result: prevState.result + "/"
          }));
            break;

          case "eql":
          const newResult = eval(this.state.result);
          this.setState( {
            result: newResult
          });
            break;

          default: //clr
            this.setState({
              result: "0"
            });
            break;
        }
        break;
    }

  }

render() {
    const numbers = ["7", "8", "9", "4", "5", "6", "3" ,"2", "1", "openParanth", "0", "closeParanth"];
    const operations = ["add", "sub", "mul", "div", "eql", "clr"];

    var numberButtons = [];
    var operationButtons = [];

    numbers.forEach((number) => {
      if(!isNaN(number)) { {/* if number */}
        numberButtons.push(
          <Button butType="n" butVal={number}  key={number} onButtonClick={this.handleButtonClick}/>
        );
      }
      else{ {/* if parantheses */}
        numberButtons.push(
          <Button butType="p" butVal={number} key={number} onButtonClick={this.handleButtonClick}/>
        );
      }
      {/* Add line breaks*/}
      if(["9", "6", "1", "closeParanth"].indexOf(number)>=0){
        numberButtons.push(<br />);
      }
    });

    operations.forEach((operation) => {
      operationButtons.push(
        <Button butType="o" butVal={operation} onButtonClick={this.handleButtonClick}/>
      );
      if(operation === "div"){
        operationButtons.push( <br/>);
      }
    });

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Calculate with React</h1>
        </header>
        <div className="App-intro">


            <CalcDisplay result={this.state.result}/>
            <form onSubmit={this.handleSubmit}>
            <input type="text" className="CalcTextInput" value={this.state.result} onChange={this.handleChange} />
            </form>
            <br />

            {numberButtons}
            {operationButtons}

        </div>
      </div>
    );
  }
}

export default App;
